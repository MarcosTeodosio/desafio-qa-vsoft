package br.org.cesar.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

import br.org.cesar.common.FormularioSimplesPage;
import br.org.cesar.common.HomePage;
import br.org.cesar.util.Utils;

/**
 * Classe de testes com cen�rios relacionados a p�gina inicial
 */
public class SearchTests extends BaseTestcase {

	@Before
	public void before() throws Exception {  
	}

	
	/**
	 * Objetivo: P�gina de resultados da busca � exibida informando nenhum resultado
	 * 
	 * Passos: Realiza busca com texto fixo e verifica o t�tulo da p�gina de resultados
	 * 
	 * Resultado Esperado: T�tulo da p�gina de resultados aponta nada encontrado
	 */
	@Test
	public void searchEnfermagem() {
		Utils.isLocated(By.cssSelector("a.widget-handle.genericon"));
		HomePage.showHiddenOptions();
		HomePage.performSearch("enfermagem");
		HomePage.clickSearchButton();
		HomePage.isHeaderTitleCorrect("Nada Encontrado");
		Utils.takeScreenshot("Erro na pesquisa");
	}
	
	
	/**
	 * Objetivo: P�gina de resultados da busca � exibida corretamente
	 * 
	 * Passos: Realiza busca com texto fixo e verifica o t�tulo da p�gina de resultados
	 * 
	 * Resultado Esperado:T�tulo da p�gina de resultados est� correto
	 */
	@Test
	public void searchTestes() {
		Utils.isLocated(By.cssSelector("a.widget-handle.genericon"));
		HomePage.showHiddenOptions();
		HomePage.performSearch("testes");
		HomePage.clickSearchButton();
		HomePage.isSearchResultCorrect("Resultados da pesquisa por: testes");
	}
	
	
	@After
	public void after() throws Exception {
	}
	
}
